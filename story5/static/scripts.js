var timeout;

function nightmode(jQuery) {
    $("#nightmode-toggle").click(function(){
       $("body").toggleClass('nightmode');
    });
}

function accordion(jQuery) {
    $("#accordion").find(".accordion-toggle").click(function() {
        $(this).next().slideToggle('fast');
        $(".accordion-content").not($(this).next()).slideUp('fast');
    });
}

function loadPage(jQuery) {
    //Console.log("Loading page");
    timeout = setTimeout(showPage, 1500);
}

function showPage(jQuery) {
    $(".loader").fadeOut();
    $("#main-content").fadeIn();
}

$(document).ready(loadPage());
$(document).ready(nightmode());
$(document).ready(accordion());