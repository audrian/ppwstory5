from django.test import TestCase, Client
from django.urls import resolve
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.color import Color
from .views import landing_page, add_status, profile_page, books_page
from .models import Status
from .forms import StatusForm

# Create your tests here.
class Story9FunctionalTest(TestCase):
	def setUp(self):
		firefox_options = webdriver.firefox.options.Options()
		firefox_options.add_argument('--headless')
		self.browser = webdriver.Firefox(firefox_options = firefox_options)

	def tearDown(self):
		self.browser.quit()

	def test_favorite_book(self):
		browser = self.browser
		browser.get('http://localhost:8000/books_page')

		try:
			star = browser.find_element_by_id('star1')
			star.click()
			compareTo = 'https://img.icons8.com/ios/50/f39c12/christmas-star-filled.png'
			self.assertEqual(star.get_attribute('innerHTML'), compareTo)
		except NoSuchElementException:
			return False;

	# ####################
	#def test_story8(self):
	#	self.browser.get('http://localhost:8000/myprofile')
	#
	#	try:
	#		nightmode_toggle = self.browser.find_element_by_id("nightmode-toggle")
	#		nightmode_toggle.click()
	#
	#		bg_color = self.browser.find_element_by_tag_name("body").value_of_css_property("background-color")
	#		bg_color_hex = Color.from_string(bg_color).hex
	#
	#		self.assertTrue(bg_color_hex, '000000')
	#	except NoSuchElementException:
	#		return False
			
class Story5Test(TestCase):
	def test_story_5_url_exists(self):
		response = Client().get('/story5/')
		self.assertEqual(response.status_code, 200)

	def test_story_5_using_landing_page_func(self):
		found = resolve('/story5/')
		self.assertEqual(found.func, landing_page)

	def test_story_5_hello_apa_kabar(self): #TODO
		response = Client().get('/story5/')
		html_response = response.content.decode('utf8')
		test = "Hello, apa kabar?"
		self.assertIn(test, html_response)

	def test_model_can_create_new_status(self):
		# Create new status
		new_status = Status.objects.create(status="Hello world!")

		# Retrieve new status
		count_status = Status.objects.all().count()
		self.assertEqual(count_status, 1)

	def test_form_validation_for_blank_items(self):
		form = StatusForm(data={'status': ''})
		self.assertFalse(form.is_valid())

	def test_story_5_profile_page_url_exists(self):
		response = Client().get('/myprofile/')
		self.assertEqual(response.status_code, 200)

	def test_story_5_profile_page_using_profile_page_function(self):
		found = resolve('/myprofile/')
		self.assertEqual(found.func, profile_page)

	def test_story_5_profile_page_has_name(self):
		response = Client().get('/myprofile/')
		html_response = response.content.decode('utf8')
		myname='Muhammad Audrian Ananda Priambodo'
		self.assertIn(myname, html_response)

	def test_story9_url_exists(self):
		response = Client().get('/books_page/')
		self.assertEqual(response.status_code, 200)

	def test_story9_using_correct_function(self):
		found = resolve('/books_page/')
		self.assertEqual(found.func, books_page)